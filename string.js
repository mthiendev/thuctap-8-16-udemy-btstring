// write a function to transform a string
// - The first letter in UPPERCASE
// - The rest inlower inlowercase
function capitalize(str) {
  if (str == "") return "";
  const firstLetter = str[0].toUpperCase();
  const res = str.slice(1).toLowerCase();
  return `${firstLetter}${res}`;
}
console.log("capitalize", capitalize("easy FRontend"));

//string exercise
// check if a string contains an email address with '@gamil.com'
function hasEmail(str) {
  return str.includes("@gmail.com");
  //   return str.indexOf("@gmail.com") != -1;
  //   return str.lastIndexOf("@gmail.com") != -1;
}
console.log("hasEmail", hasEmail("dadah@gmail.com"));

//string exercise
//write a JS function to parameterize a string
// Eg: Code JS Is Fun =>  code-js-js-fun

function paramateriz(str) {
  return str.toLowerCase().replaceAll(" ", "-");
  //   return str.toLowerCase().split(" ").join("-");
}
console.log("paramateriz: ", paramateriz("Code JS Is Fun"));

// Create a function 'truncate(text,maxlength)' that check the length of the text and
//if it exceeds maxlength -replaces th end of str with the ellipslis character "_"
// to  make its length equal to maxlength
function truncate(text, maxlength) {
  if (text.length <= maxlength) return text;
  const shortStr = text.slice(0, maxlength - 1);
  return `${shortStr}\u2026`;
}
console.log("truncate", truncate("Easy frontend", 5));

// Tìm và xóa các nguyên âm trong câu văn
function removeVowel(str) {
  return str.replace(/a|i|u|e|o/gi, "").trim();
}
console.log("removeVowel: ", removeVowel("hello world"));

// format số giây luôn hiển thi 2 số
function formatSecondsV1(seconds) {
  if (typeof seconds !== "number" || seconds < 0 || seconds > 60) return -1;
  if (seconds >= 10) return `${seconds}`;
  return `0${seconds}`;
}
console.log("formatSecondsV1: ", formatSecondsV1(00));
function formatSecondsV2(seconds) {
  if (typeof seconds !== "number" || seconds < 0 || seconds > 60) return -1;
  const secondsAddZero = `0${seconds}`;
  const START_INDEX = -2;
  return secondsAddZero.slice(START_INDEX);
}
console.log("formatSecondsV2: ", formatSecondsV2(30));

// convert số giây sang chuỗi hh:mm:ss
const SECONDS_PER_HOUR = 3600;
const SECONDS_PER_MIN = 60;
function formatTime(seconds) {
  if (typeof seconds !== "number" || seconds < 0 || seconds > 86400) return -1;
  const hour = formatSecondsV1(Math.floor(seconds / SECONDS_PER_HOUR));
  const minute = formatSecondsV1(Math.floor((seconds % SECONDS_PER_HOUR) / SECONDS_PER_MIN));
  const second = formatSecondsV1(Math.floor((seconds % SECONDS_PER_HOUR) % SECONDS_PER_MIN));
  return `${hour}:${minute}:${second}`;
}
console.log("formatTime: ", formatTime(3800));
